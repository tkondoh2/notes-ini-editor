﻿#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QCoreApplication>

struct Parameters
{
  QString command;
  QString name;
  QString value;

  static Parameters parse(QCoreApplication &app);
};

#endif // PARAMETERS_H
