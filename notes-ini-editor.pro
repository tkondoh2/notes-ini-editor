QT -= gui

CONFIG += c++11 c++14 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

win32: QMAKE_CXXFLAGS += /utf-8

include(../environment.pri)

#
# RxCpp
#
INCLUDEPATH += $$RxCppPath/Rx/v2/src
DEPENDPATH += $$RxCppPath/Rx/v2/src

#
# Notes C API
#
NotesCAPIPath = Y:/notesapi
include($$NotesCAPIPath/qt_notes_c_api.pri)

INCLUDEPATH += $$PWD/..
DEPENDPATH += $$PWD/..

SOURCES += \
        main.cpp \
        mainprocess.cpp \
        parameters.cpp

#  notes/env.hpp \
#  notes/lmbcs.hpp \
#  notes/lmbcslist.hpp \
#  notes/session.hpp \
#  notes/status.hpp \
#  notes/translate.hpp \
HEADERS += \
  mainprocess.h \
  parameters.h
