﻿#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include "parameters.h"

#include <QTextStream>

struct MainProcess
{
  int operator ()(const Parameters &params);
};

#endif // MAINPROCESS_H
