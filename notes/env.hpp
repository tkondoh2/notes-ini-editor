﻿#ifndef NOTES_ENV_HPP
#define NOTES_ENV_HPP

#include "notes/lmbcslist.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osenv.h>
#include <osfile.h>

#ifdef NT
#pragma pack(pop)
#endif

namespace notes {
namespace env {

inline Lmbcs getString(const Lmbcs &key, const Lmbcs &defaultValue = "")
{
  char buffer[MAXENVVALUE];
  return !OSGetEnvironmentString(key.constData(), buffer, MAXENVVALUE - 1)
      ? defaultValue
      : Lmbcs(buffer);
}

inline long getLong(const Lmbcs &key, long defaultValue = 0)
{
  long value = OSGetEnvironmentLong(key.constData());
  return value == 0 ? defaultValue : value;
}

inline int getInteger(const Lmbcs &key, int defaultValue = 0)
{
  int value = OSGetEnvironmentInt(key.constData());
  return value == 0 ? defaultValue : value;
}

inline void setString(const Lmbcs &key, const Lmbcs &value)
{
  OSSetEnvironmentVariable(key.constData(), value.constData());
}

inline void clear(const Lmbcs &key)
{
  setString(key, "");
}

inline void appendItem(const Lmbcs &key, const Lmbcs &value, char delimiter = ',')
{
  Lmbcs v = getString(key);
  if (v.isEmpty()) {
    setString(key, value);
  }
  else {
    LmbcsList list = notes::split(v, delimiter);
    if (!list.contains(value)) {
      list.append(value);
    }
    setString(key, list.join(delimiter));
  }
}

inline void removeItem(const Lmbcs &key, const Lmbcs &value, char delimiter = ',')
{
  LmbcsList list = notes::split(getString(key), delimiter);
  list.removeAll(value);
  setString(key, list.join(delimiter));
}

} // namespace env
} // namespace notes

#endif // NOTES_ENV_HPP
