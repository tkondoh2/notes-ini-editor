﻿#ifndef NOTES_SESSION_HPP
#define NOTES_SESSION_HPP

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // STATUS,NotesInitExtended,NotesTerm

#ifdef NT
#pragma pack(pop)
#endif

namespace notes {

struct Session
{
  template <typename Func>
  int operator ()(int argc, char *argv[], Func func)
  {
    int retCode = -1;
    STATUS status = NotesInitExtended(argc, argv);
    if (status == NOERROR) {
      retCode = func(argc, argv);
      NotesTerm();
    }
    return retCode;
  }
};

} // namespace notes

#endif // NOTES_SESSION_HPP
