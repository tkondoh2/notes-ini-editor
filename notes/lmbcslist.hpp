﻿#ifndef NOTES_LMBCSLIST_HPP
#define NOTES_LMBCSLIST_HPP

#include "notes/lmbcs.hpp"

#include <QList>
#include <QVector>
#include <QSet>

namespace notes {

class LmbcsList
    : public QList<Lmbcs>
{
public:
  LmbcsList() {}

  Lmbcs join(char sep) const
  {
    QByteArray bytes;
    if (count() > 0) {
      bytes += first().value();
      for (int i = 1; i < count(); ++i) {
        bytes += sep;
        bytes += at(i).value();
      }
    }
    return Lmbcs(std::move(bytes));
  }

  static LmbcsList fromQStringList(const QStringList &qstrlist)
  {
    LmbcsList list;
    foreach (QString item, qstrlist) {
      list.append(Lmbcs::fromQString(item));
    }
    return list;
  }
};

inline LmbcsList split(const Lmbcs &str, char sep)
{
  QByteArray v = str.value();
  QByteArrayList l = v.split(sep);
  LmbcsList list;
  foreach (QByteArray i, l) {
    list.append(Lmbcs(i));
  }
  return list;
}

} // namespace notes

#endif // NOTES_LMBCSLIST_HPP
