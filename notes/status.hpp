﻿#ifndef NOTES_STATUS_HPP
#define NOTES_STATUS_HPP

#include <QByteArray>

#include <rxcpp/rx.hpp>
using namespace rxcpp;
using namespace rxcpp::operators;

#ifdef NT
#pragma pack(push, 1)
#endif

#include <global.h> // STATUS

#ifdef NT
#pragma pack(pop)
#endif

namespace notes {

} // namespace notes

#endif // NOTES_STATUS_HPP
