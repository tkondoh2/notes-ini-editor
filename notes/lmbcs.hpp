﻿#ifndef NOTES_LMBCS_HPP
#define NOTES_LMBCS_HPP

#include "notes/translate.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <nls.h> // NLS_string_chars

#ifdef NT
#pragma pack(pop)
#endif

namespace notes {

class Lmbcs
{
  QByteArray value_;

public:
  Lmbcs() noexcept : value_() {}

  Lmbcs(const char *c_str, int size = -1) noexcept
    : value_(c_str, size)
  {}

  Lmbcs(QByteArray &&bytes) noexcept
    : value_(std::move(bytes))
  {}

  const QByteArray &value() const noexcept { return value_; }
  const char *constData() const noexcept { return value_.constData(); }
  bool isEmpty() const noexcept { return value_.isEmpty(); }

  int size() const noexcept
  {
    QByteArray s = value_.left(MAXWORD - 1);
    WORD numChars = 0;
    NLS_STATUS status = NLS_string_chars(
          reinterpret_cast<const BYTE*>(s.constData())
          , NLS_NULLTERM
          , &numChars
          , OSGetLMBCSCLS()
          );
    if (status != NLS_SUCCESS) return -1;

    return static_cast<int>(numChars);
  }

  QString qstr() const noexcept
  {
    // 必要なバッファサイズを割り出す。
    int bufferSize = size() * sizeof(ushort);
    if (bufferSize < 0) return QString();

    // 関数オブジェクトLmbcsToUnicodeでUnicode(UTF-16)に変換する。
    QByteArray utf16 = LmbcsToUnicode()(value(), bufferSize);

    // バイナリ状のUTF-16からQStringオブジェクトを作成する。
    return QString::fromUtf16(
          reinterpret_cast<ushort*>(const_cast<char*>(utf16.constData())),
          utf16.size() / sizeof(ushort)
          );
  }

  /**
   * @brief LmbcsList対策で必要な二項演算子は自由関数ではなくメンバ関数だった件
   * @param other
   * @return
   */
  bool operator ==(const Lmbcs &other) const noexcept
  {
    return value() == other.value();
  }

  bool operator !=(const Lmbcs &other) const noexcept
  {
    return !operator ==(other);
  }

  static Lmbcs fromQString(const QString &qstr) noexcept
  {
    int bufferSize = qstr.size() * 3;

    // QStringをUnicode(UTF-16)にする。
    QByteArray utf16(
          reinterpret_cast<const char*>(qstr.utf16()),
          qstr.size() * 2
          );

    // LMBCS化してLmbcsオブジェクトに変換して返す。
    return Lmbcs(UnicodeToLmbcs()(utf16, bufferSize));
  }
};

inline Lmbcs ql(const QString &qstr) { return Lmbcs::fromQString(qstr); }

} // namespace notes

#endif // NOTES_LMBCS_HPP
