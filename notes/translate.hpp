﻿#ifndef NOTES_TRANSLATE_HPP
#define NOTES_TRANSLATE_HPP

#include "notes/status.hpp"

#ifdef NT
#pragma pack(push, 1)
#endif

#include <osmisc.h>

#ifdef NT
#pragma pack(pop)
#endif

#define MAX_TRANSLATE_SIZE (MAXWORD - 1)

namespace notes {

// g++ではダングリングによるランタイムエラーになってしまうため、独自に定義。

template <typename T>
inline T min(T a, T b) { return (b < a) ? b : a; }

template <typename T>
inline T max(T a, T b) { return (b > a) ? b : a; }

/**
 * @brief 指定したモードで文字列変換する関数オブジェクト
 */
struct Translate
{
  QByteArray operator ()(
      WORD mode,
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
      ) noexcept
  {
    int size = min<int>(source.size(), MAX_TRANSLATE_SIZE);
    int bufSize = min<int>(
          max<int>(bufferSize, 16)
          , MAX_TRANSLATE_SIZE
          );
    QByteArray buffer(bufSize, '\0');
    WORD len = OSTranslate(
          mode
          , source.constData()
          , static_cast<WORD>(size)
          , buffer.data()
          , static_cast<WORD>(bufSize)
          );
    return buffer.left(static_cast<int>(len));
  }
};

/**
 * @brief UNICODE(UTF-16)をLMBCS文字列に変換する。
 * @param source UNICODE(UTF-16)文字列
 * @param bufferSize バッファーサイズ
 * @return LMBCS文字列
 */
struct UnicodeToLmbcs
{
  QByteArray operator ()(
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
      ) noexcept
  {
    return Translate()(OS_TRANSLATE_UNICODE_TO_LMBCS, source, bufferSize);
  }
};

/**
 * @brief LMBCS文字列をUNICODE(UTF-16)に変換する。
 * @param source LMBCS文字列
 * @param bufferSize バッファーサイズ
 * @return UNICODE(UTF-16)文字列
 */
struct LmbcsToUnicode
{
  QByteArray operator ()(
      const QByteArray &source,
      int bufferSize = MAX_TRANSLATE_SIZE
      ) noexcept
  {
    return Translate()(OS_TRANSLATE_LMBCS_TO_UNICODE, source, bufferSize);
  }
};

} // namespace notes

#endif // NOTES_TRANSLATE_HPP
