﻿#include "mainprocess.h"

//#include "notes/env.hpp"
#include <gouda/notes/env.hpp>

namespace cn {
//using namespace notes;
using namespace gouda::notes;
}

int MainProcess::operator ()(const Parameters &params)
{
  QTextStream qout(stdout);

  // コマンドに沿って関数を実行する。
  if (params.command == "set") {
    cn::env::setString(cn::ql(params.name), cn::ql(params.value));
    qout << QObject::tr("Environment variable %1 was set to %2.")
         .arg(params.name)
         .arg(params.value)
         << endl;
  }
  else if (params.command == "unset") {
    cn::env::clear(cn::ql(params.name));
    qout << QObject::tr("Environment variable %1 has been invalidated.")
         .arg(params.name)
         << endl;
  }
  else if (params.command == "append") {
    cn::env::appendItem(cn::ql(params.name), cn::ql(params.value));
    qout << QObject::tr("Added %2 to the list of"
                        " environment variables %1.")
         .arg(params.name)
         .arg(params.value)
         << endl;
  }
  else if (params.command == "remove") {
    cn::env::removeItem(cn::ql(params.name), cn::ql(params.value));
    qout << QObject::tr("Removed %2 from the list of"
                        " environment variables %1.")
         .arg(params.name)
         .arg(params.value)
         << endl;
  }
  else if (params.command == "show") {
//    qout << cn::env::getString(cn::ql(params.value)).qstr() << endl;
    qout << cn::lq(cn::env::getString(cn::ql(params.value))) << endl;
  }
  else {
    qout << QObject::tr("Unknown command.") << endl;
    return 1;
  }

  return 0;
}
