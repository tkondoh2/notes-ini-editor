﻿#include "parameters.h"

#include <QCommandLineParser>
#include <QCommandLineOption>

Parameters Parameters::parse(QCoreApplication &app)
{
  // コマンドラインパーサを作成する。
  QCommandLineParser parser;

  // ヘルプオプションを追加する。
  parser.addHelpOption();

  // バージョンオプションを追加する。
  parser.addVersionOption();

  // 引数1(コマンド)を追加する。
  parser.addPositionalArgument(
        "command",
        QObject::tr("Specify command (show,set,unset,append,remove).")
        );

  // 引数2(変数名)を追加する。
  parser.addPositionalArgument(
        "key",
        QObject::tr("Specify Notes environment key.")
        );

  // オプション引数(変数値)を追加する。
  QCommandLineOption valueOption(
        QStringList() << "V" << "value",
        QObject::tr("Specify Notes environment value"
                    " (In the case of \"append\" or \"remove\","
                    " the values are treated"
                    " like a comma separated list.)."
                    ),
        "value"
        );
  parser.addOption(valueOption);

  // コマンドラインをパースする。
  parser.process(app);
  QStringList arglist = parser.positionalArguments();

  Parameters args;

  // コマンドを取得する。
  if (arglist.count() >= 1)
    args.command = arglist.first().toLower();

  // 変数名を取得する。
  if (arglist.count() >= 2)
    args.name = arglist.at(1);

  // あれば変数値を取得する。
  if (parser.isSet(valueOption))
  args.value = parser.value(valueOption);

  return args;
}
