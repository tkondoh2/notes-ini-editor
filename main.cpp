﻿#include <QCoreApplication>
#include <QTimer>

//#include "notes/session.hpp"
#include <gouda/notes/session.hpp>
#include "mainprocess.h"

namespace cn {
//using namespace notes;
using namespace gouda::notes;
}

int main(int argc, char *argv[])
{
  return cn::Session()(argc, argv, [](int argc, char *argv[])
  {
    // Qt Applicationを初期化する。
    QCoreApplication app(argc, argv);
    QTimer::singleShot(0, [&]() {
      Parameters params = Parameters::parse(app);
      MainProcess()(params);
      app.exit(0);
    });
    return app.exec();
  });
}
